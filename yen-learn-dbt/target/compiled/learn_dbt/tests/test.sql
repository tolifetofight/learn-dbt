
with dbt__CTE__INTERNAL_test as (
select sum(c_acctbal) as total_c_acctbal
from analytics.dbt.playing_with_tests
--where c_mktsegment in ('BUILDING', 'AUTOMOBILE', 'MACHINERY', 'HOUSEEHOLD','FURNITURE')
having sum(c_acctbal) < 100000000
)select count(*) from dbt__CTE__INTERNAL_test