
with cte
as
(
  select o_orderdate,
      sum(o_totalprice) as o_totalprice
  from snowflake_sample_data.TPCH_SF1.orders
  group by o_orderdate
  order by o_orderdate
  )
  select o_orderdate,
    sum(o_totalprice) over(order by o_orderdate rows between unbounded preceding and current row) as cum_sales
  from cte