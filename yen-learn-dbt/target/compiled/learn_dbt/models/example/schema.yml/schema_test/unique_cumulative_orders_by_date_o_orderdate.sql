
    
    



select count(*) as validation_errors
from (

    select
        o_orderdate

    from analytics.dbt.cumulative_orders_by_date
    where o_orderdate is not null
    group by o_orderdate
    having count(*) > 1

) validation_errors


